var guestlist = Alloy.Collections.guestlist,
  Backbone = require('alloy/backbone'),
  cloneGuests = new Backbone.Collection(guestlist.toJSON()),
  checkTicket = require('checkTicket'),
  guestsFull = require('guestsFull'),
  guestsSearch = require('guestsSearch'),
  ticketCount = require('ticketCount'),
  allLoaded = false,
  searchMode = false,
  canScroll = true,
  totalRecords = 0,
  waitForPause,
  pauseDelay = 1000,
  pages;

var mode = function() {
  return Titanium.App.Properties.getString("app:mode");
};

var isOnline = function() {
  if (Ti.Network.networkType === Ti.Network.NETWORK_NONE) {
    return false;
  } else {
    return true;
  }
};

var loadAllGuests = function(nextPage) {
  if (OS_ANDROID) {

  } else {
    Titanium.App.idleTimerDisabled = true;
  }

  var pageNo = nextPage + 1 || 1;
  var getGuests = guestsFull(pageNo, function(resp, err) {
    var batch = resp.guestlist;
    if (batch.length > 0) {
      var db = Ti.Database.open('_alloy_');
      db.execute('BEGIN');
      // begin the transaction
      for (var i = 0,
          j = batch.length; i < j; i++) {
        var item = batch[i];
        db.execute('INSERT OR REPLACE INTO guestlist (first_name, last_name, quantity, serial_number, checked, event_id, item_id) VALUES (?, ?, ?, ?, ?, ?, ?)', item.first_name, item.last_name, item.quantity, item.serial_number, item.checked, item.event_id, item.item_id);
      }
      db.execute('COMMIT');
      var progress;
      if (pageNo == pages) {
        if (OS_ANDROID) {

        } else {
          Titanium.App.idleTimerDisabled = false;
        }
        guestlist.fetch({localOnly: true});
        cloneGuests.add(guestlist.toJSON());
        progress = 0;
        $.loading.progress(progress);
        $.loading.hide();
      } else {
        progress = ((pageNo * 25) / totalRecords) * 100;
        $.loading.progress(progress);
        loadAllGuests(pageNo);
      }
      db.close();
      cloneGuests.add(batch);
    }
  });
};

var loadSavedGuests = function() {
  guestlist.fetch({
    sql: {
      where: {
        event_id: Titanium.App.Properties.getInt("app:event")
      }
    },
    localOnly: true,
    success: function(collection, response, options) {
      alert('You have no internet connection, so saved guests have been loaded.');
      cloneGuests.add(response);
      $.loading.hide();
    }
  });
};

function loadGuests(next) {
  if (!canScroll || !isOnline()) {
    return;
  }
  canScroll = false;
  var ln = guestlist.models.length;
  guestlist.fetch({
    url: Alloy.Globals.getApi() + "events/" + Titanium.App.Properties.getInt("app:event") + "/guestlist",
    urlparams: {
      user: Titanium.App.Properties.getString("email"),
      token: Titanium.App.Properties.getString("token"),
      offset: ln
    },
    add: next,
    silent: next,
    timeout: 60000,
    success: function(collection, response, options) {
      if (collection.models.length === totalRecords || response.length === 0) {
        allLoaded = true;
      }
      canScroll = true;
      cloneGuests.add(response);
      $.loading.hide();
    },
    error: function(collection, response, options) {
      canScroll = true;
      $.loading.hide();
    }
  });
}

Ti.App.addEventListener('app:scanned', function(ticket) {
  var data = {
    event_id: Titanium.App.Properties.getInt("app:event"),
    first_name: ticket.first_name,
    last_name: ticket.last_name,
    quantity: ticket.quantity,
    serial_number: ticket.serial_number,
    checked: true,
    item_id: ticket.item_id
  };
  guestlist.add(data);
  var guest = guestlist.get(ticket.serial_number);
  guest.set({
    checked: true,
  });
  cloneGuests.add(data);
  if (mode() === 'online') {
    updateCount(function() {});
  }
});

Ti.App.addEventListener('app:scanned:offline', function(guest) {
  cloneGuests.add(guest);
});

function cloneUpdater(serial) {
  var clone = cloneGuests.where({
    serial_number: serial
  });
  clone[0].set({
    checked: true
  });
}

function processInput() {
  $.searchLoading.top = 0;
  $.actInd.show();
  guestlist.reset();
  if ($.search.value === "" || $.search.value === null) {
    guestlist.reset(cloneGuests.toJSON());
    searchMode = false;
  } else {
    var keyword = $.search.value.toLowerCase();
    if (Titanium.Network.online && mode() === 'online') {
      searchMode = true;
      var getGuests = guestsSearch(keyword, function(resp, err) {
        var batch = resp.guestlist;
        var db = Ti.Database.open('_alloy_');
        db.execute('BEGIN');
        // begin the transaction
        for (var i = 0,
            j = batch.length; i < j; i++) {
          var item = batch[i];
          db.execute('INSERT OR REPLACE INTO guestlist (first_name, last_name, quantity, serial_number, checked, event_id, item_id) VALUES (?, ?, ?, ?, ?, ?, ?)', item.first_name, item.last_name, item.quantity, item.serial_number, item.checked, item.event_id, item.item_id);
        }
        db.execute('COMMIT');
        db.close();
        guestlist.reset(batch);
        $.searchLoading.top = -60;
        $.actInd.hide();
        cloneGuests.add(batch);
      });
    } else {
      var collection = cloneGuests.filter(function(guest) {
        if (guest.get('first_name').toLowerCase().indexOf(keyword) != -1 || guest.get('last_name').toLowerCase().indexOf(keyword) != -1)
          return guest;
      });
      $.searchLoading.top = -60;
      $.actInd.hide();
      guestlist.reset(collection);
    }
  }
}

$.search.addEventListener('change', function() {
  clearTimeout(waitForPause);
  if ($.search.value.length > 2) {
    waitForPause = setTimeout(processInput, pauseDelay);
  }
});

$.search.addEventListener('return', function(e) {
  $.search.blur();
  processInput();
});

$.search.addEventListener('cancel', function(e) {
  $.search.value = '';
  $.search.blur();
  guestlist.reset(cloneGuests.toJSON());
  searchMode = false;
  onMarkerEvent();
});

if (OS_ANDROID) {
  $.search.addEventListener('click', function(e) {
    $.search.setSoftKeyboardOnFocus(Ti.UI.Android.SOFT_KEYBOARD_SHOW_ON_FOCUS);
    $.search.focus();
  });
}

function onMarkerEvent() {
  if (!allLoaded && mode() === 'online' && !searchMode && isOnline()) {
    var sectionIndex = Math.max(0, $.listView.sectionCount - 1);
    var itemIndex = Math.max($.listView.sections[sectionIndex].items.length - 1);
    loadGuests(true);
    $.listView.setMarker({
      sectionIndex: sectionIndex,
      itemIndex: itemIndex
    });
  }
}

function transformGuest(model) {
  var transform = model.toJSON();
  transform.full_name = transform.first_name + ' ' + transform.last_name;
  if (OS_ANDROID) {
    if (transform.checked == 'true') {
      transform.template = "ticketUsedTemplate";
      transform.color = "#d5d7d7";
    } else {
      transform.template = "ticketTemplate";
      transform.color = "white";
    }
  } else {
    if (transform.checked) {
      transform.template = "ticketUsedTemplate";
      transform.color = "#d5d7d7";
    } else {
      transform.template = "ticketTemplate";
      transform.color = "white";
    }
  }
  return transform;
}

var openScannerIfPermission = function() {
  $.search.value = '';
  $.search.blur();
  searchMode = false;
  guestlist.reset(cloneGuests.toJSON());
  var scanner = Alloy.createController('scanner').getView();
  scanner.open();


};

function openScanner() {
  if (Ti.Media.hasCameraPermissions()) {
      openScannerIfPermission();
  } else {
      Ti.Media.requestCameraPermissions(function(e) {
          if (e.success) {
              openScannerIfPermission();
          } else {
              alert('You denied camera permission.');
          }
      });
  }
}

//Guest selected
var showButton = false;

function guestSelected(e) {
  var item = e.section.getItemAt(e.itemIndex);
  if (showButton) {
    showButton = false;
    item.container.left = -60;
  } else {
    showButton = true;
    item.container.left = 0;
  }
  e.section.updateItemAt(e.itemIndex, item);
}

function confirmListTicket(e) {
  $.loading.show();
  var item = e.section.getItemAt(e.itemIndex);
  var serial_number = item.properties.itemId;
  var dialog;
  if (Titanium.Network.online && mode() === 'online') {
    var guest = guestlist.get(serial_number);
    checkTicket(serial_number, function(resp, err) {
      if (err) {
        $.loading.hide();
        return alert(err);
      }
      $.loading.hide();
      switch (resp.response) {
        case "DUPLICATE":
          dialog = Ti.UI.createAlertDialog({
            message: "This ticket has already been scanned on another device.",
            title: "Ticket Used",
          }).show();
          guest.set({
            checked: true
          });
          cloneUpdater(serial_number);
          break;
        case "OK":
          guest.set({
            checked: true
          });
          cloneUpdater(serial_number);
          break;
        case "NOT FOUND":
          dialog = Ti.UI.createAlertDialog({
            message: "Ticket not found",
            title: "Ticket Invalid",
          }).show();
          break;
      }
      if (OS_ANDROID) {
        $.search.value = "";
        $.search.hide();
        $.search.show();
      }
      if (mode() === 'online') {
        updateCount(function() {});
      }
    });
  } else {
    var foundGuest = guestlist.get(serial_number);
    foundGuest.set({
      checked: true
    });
    $.loading.hide();
    cloneUpdater(serial_number);
  }
}

$.listView.setMarker({
  sectionIndex: 0,
  itemIndex: 10
});

//Init guestlist depending of mode

function initGuests() {
  var getTotal;
  if (!isOnline() && mode() == 'offline') {
    //Offline
    $.ticketCount.text = "You are in offline mode";
    loadSavedGuests();
  } else if (isOnline() && mode() == 'offline') {
    updateCount(function() {
      loadAllGuests();
    });

  } else {
    //Online
    updateCount(function() {
      loadGuests(false);
    });
  }
}

function updateCount(callback) {
  var getTotal;
  if (!isOnline()) {
    //Offline
    $.ticketCount.text = "You are in offline mode";
  } else {
    //Online
    getTotal = ticketCount(function(resp, err) {
      if (err) {
        $.loading.hide();
        return alert('There was an error connecting to the server. Please check you have a data connection and try again.');
      }
      totalRecords = resp.event.total_guestlist_records;
      pages = Math.ceil(totalRecords / 25);
      $.ticketCount.text = "Scanned " + resp.event.tickets_checked + "/" + resp.event.tickets_sold;
      callback();
    });
  }
}

$.guestlist.addEventListener('open', function(e) {
  $.loading.show();
  initGuests();
});

$.guestlist.addEventListener('close', function(e) {
  if (mode() == 'online') {
    guestlist.reset();
  }
  $.destroy();
});

Ti.Network.addEventListener('change', function(e) {
  if (Ti.Network.networkType === Ti.Network.NETWORK_NONE && mode() === 'online') {
    alert('Internet connection lost. The app will continue to work but ticket checks will only be saved to this device.');
  }
});
