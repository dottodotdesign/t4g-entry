var mode = Titanium.App.Properties.getString("app:mode");
$.loading.hide();

function setColor(mode) {
	if (mode === 'online') {
		$.online.backgroundColor = '#78cb20';
		$.offline.backgroundColor = 'white';
	} else {
		$.online.backgroundColor = 'white';
		$.offline.backgroundColor = '#78cb20';
	}
}

setColor(mode);

function setOnline() {
	Titanium.App.Properties.setString("app:mode", "online");
	Titanium.App.Properties.setBool('app:online', false);
	setColor("online");
}

function setOffline() {
	Titanium.App.Properties.setString("app:mode", "offline");
	Titanium.App.Properties.setBool('app:online', true);
	setColor("offline");
}

function closeWindow() {
	$.popup.close();
}