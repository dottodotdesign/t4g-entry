var scanditsdk = require("com.mirasense.scanditsdk");
scanditsdk.appKey = "bMBG6DAkEeSaGHchdqqg20qiAaXAtAHmh4uyll2Szm0";
scanditsdk.cameraFacingPreference = 0;
var guestlist = Alloy.Collections.guestlist,
  checkTicket = require('checkTicket'),
  mode = Titanium.App.Properties.getString("app:mode");

var picker = scanditsdk.createView({
  width: "100%",
  height: "100%"
});
$.loading.hide();
picker.init();

var isOnline = function() {
  if (Ti.Network.networkType === Ti.Network.NETWORK_NONE) {
    return false;
  } else {
    return true;
  }
};

function scanNext() {
  picker.startScanning();
  $.ticket.visible = false;
  $.ticket.enabled = false;
  $.ticket_type.text = "";
}

function markOk() {
  $.ticket.visible = true;
  $.ticket.enabled = true;
  $.check.backgroundImage = "/images/tickLarge.png";
  $.ticket.backgroundColor = "#78cb20";
}

function markUsed() {
  $.ticket.visible = true;
  $.ticket.enabled = true;
  $.qty.text = "TICKET USED";
  $.check.backgroundImage = "/images/crossLarge.png";
  $.ticket.backgroundColor = "#C1272D";
}

function notFound() {
  $.ticket.visible = true;
  $.ticket.enabled = true;
  $.full_name.color = "#3C3C3B";
  $.full_name.text = "TICKET NOT FOUND";
  $.qty.text = "";
  $.check.backgroundImage = "/images/crossLarge.png";
  $.ticket.backgroundColor = "#C1272D";
}

picker.setSuccessCallback(function(e) {
  var scannedCode = e.barcode;
  picker.stopScanning();
  if (isOnline()) {
    setTimeout(function() {
      $.loading.show();
      checkTicket(scannedCode, function(resp, err) {
        if (err) {
          $.loading.hide();
          Ti.API.info('error');
          picker.startScanning();
          notFound();
          return;
        }

        $.loading.hide();
        var ticket = resp.ticket;
        switch (resp.response) {
          case "DUPLICATE":
            $.full_name.text = ticket.first_name + ' ' + ticket.last_name;
            $.qty.text = 'Admits ' + ticket.quantity;
            if (ticket.ticket_type) {
              $.ticket_type.text = 'Ticket type: ' + ticket.ticket_type;
            }
            markUsed();
            Ti.App.fireEvent('app:scanned', ticket);
            break;
          case "OK":
            $.full_name.text = ticket.first_name + ' ' + ticket.last_name;
            $.qty.text = 'Admits ' + ticket.quantity;
            if (ticket.ticket_type) {
              $.ticket_type.text = 'Ticket type: ' + ticket.ticket_type;
            }
            markOk();
            Ti.App.fireEvent('app:scanned', ticket);
            break;
          case "NOT FOUND":
            notFound();
            break;
          default:
            notFound();
        }
      });
    }, 0);
  } else {
    var guest = guestlist.get(e.barcode);
    if (guest) {
      var foundTicket = guest.toJSON();
      $.full_name.text = foundTicket.first_name + ' ' + foundTicket.last_name;
      if (foundTicket.ticket_type) {
        $.ticket_type.text = 'Ticket type: ' + foundTicket.ticket_type;
      }
      if (foundTicket.checked) {
        markUsed();
      } else {
        $.qty.text = 'Admits ' + foundTicket.quantity;
        markOk();
        guest.set({
          checked: true,
        });
        Ti.App.fireEvent('app:scanned:offline', guest.toJSON());
      }
    } else {
      notFound();
    }
  }
});

$.scandit.add(picker);

$.scandit.addEventListener('open', function(e) {
  picker.setSize(Ti.Platform.displayCaps.platformWidth, Ti.Platform.displayCaps.platformHeight);
  picker.startScanning();
});

function closeWindow() {
  if (picker !== null) {
    picker.stopScanning();
    $.scandit.remove(picker);
  }
  if (OS_ANDROID) {
    $.scandit.close();
  } else {
    $.popup.close();
  }
  $.destroy();
}
