Alloy.CFG.nav = $.nav;
var moment = require('alloy/moment'),
    events = Alloy.Collections.events,
    guestlist = Alloy.Collections.guestlist,
    xpng = require('xpng');

$.loading.hide();

Ti.App.addEventListener('app:login', checkLogin);
Ti.App.addEventListener('app:reload', checkLogin);

if (OS_ANDROID) {
	$.index.open();
} else {
	$.nav.open();
}

var mode = function() {
	return Titanium.App.Properties.getString("app:mode");
};

var showLogin = function () {
	var newWin = Alloy.createController('login').getView();
	newWin.open({
		animate : false
	});
};

function loadEvents() {
	Ti.API.info('load events');
	$.mode.text = mode().toUpperCase() + " MODE";
	$.loading.show();
	events.fetch({
		url : Alloy.Globals.getApi() + "events",
		urlparams : {
			user : Titanium.App.Properties.getString("email"),
			token : Titanium.App.Properties.getString("token")
		},
		localOnly : Titanium.App.Properties.getBool('app:online'),
		success : function(_model, _response) {
			$.loading.hide();
			if (_response.response == "Not Authorised") {
				showLogin();
			}
		},
		error : function(_model, _response) {
			Ti.API.info('Events Fetch Error');
			Ti.API.info(_response);
			$.loading.hide();
			events.reset();
		}
	});
}

function checkLogin() {
	if (Titanium.App.Properties.getString("email")) {
		if (!Titanium.Network.online && mode() === 'online') {
			eventNetworkError();
		}
		if (mode() === 'online') {
			var db = Ti.Database.open('_alloy_');
			db.execute('DELETE FROM guestlist');
			db.close();
			guestlist.reset();
		}
		loadEvents();
	} else {
		$.loading.hide();
		showLogin();
	}
}

checkLogin();

function resetPullHeader() {
	$.actInd.hide();
	$.imageArrow.transform = Ti.UI.create2DMatrix();
	$.imageArrow.show();
	$.listEvents.setContentInsets({
		top : 0
	}, {
		animated : true
	});
}

function reloadEvents() {
	Ti.API.info('reload events');
	events.fetch({
		url : Alloy.Globals.getApi() + "events",
		urlparams : {
			user : Titanium.App.Properties.getString("email"),
			token : Titanium.App.Properties.getString("token")
		},
		local : Titanium.App.Properties.getBool('app:online'),
		success : function(_model, _response) {
			resetPullHeader();
		},
		error : function(_model, _response) {
			Ti.API.info('Events Fetch Error');
			Ti.API.info(_response);
			resetPullHeader();
		}
	});
}

function pullListener(e) {
	if (e.active === false) {
		var unrotate = Ti.UI.create2DMatrix();
		$.imageArrow.animate({
			transform : unrotate,
			duration : 180
		});
	} else {
		var rotate = Ti.UI.create2DMatrix().rotate(180);
		$.imageArrow.animate({
			transform : rotate,
			duration : 180
		});
	}
}

function pullendListener(e) {
	$.imageArrow.hide();
	$.actInd.show();
	$.listEvents.setContentInsets({
		top : 60
	}, {
		animated : true
	});
	setTimeout(function() {
		if (!Titanium.Network.online && mode() === 'online') {
			eventNetworkError();
		}
		reloadEvents();
	}, 1000);
}

$.listEvents.addEventListener('pull', pullListener);
$.listEvents.addEventListener('pullend', pullendListener);

function showSettings() {
	var settings = Alloy.createController('settings').getView();
	settings.open();
}

Ti.App.addEventListener('settings:closed', function(e) {
  checkLogin();
});

function transformEvent(model) {
	var transform = model.toJSON();
	transform.details = moment(transform.start_date).format("ddd D MMM h:mma");
	transform.tickets_sold = "Tickets sold: " + transform.tickets_sold;
	return transform;
}

function showEvent(e) {
	var event = $.events.getItemAt(e.itemIndex);
	var id = event.properties.itemId;
	Titanium.App.Properties.setInt("app:event", id);
	if (mode() === 'offline') {
		$.alertDialog.show();
	} else {
		xpng.openWin(Alloy.CFG.nav, "guestlist");
	}
}

$.alertDialog.addEventListener('click', function(e) {
	if (e.index === 0) {
		xpng.openWin(Alloy.CFG.nav, "guestlist");
	}
});

function changeMode() {
	var modeSelector = Alloy.createController('mode').getView();
	modeSelector.open();
	modeSelector.addEventListener('close', function(e) {
		setTimeout(function() {
			checkLogin();
		}, 100);
	});
}

function eventNetworkError() {
	return alert('No network connection found. Please connect to the internet and try again to get your updated events list.');
}
