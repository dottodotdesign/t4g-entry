var passwordKeychainItem = Alloy.Globals.passKeychainItem;
$.email.text = Titanium.App.Properties.getString("email");
$.version.text = 'v' + Ti.App.version;
function closeWindow() {
	Ti.App.fireEvent('settings:closed');
	$.popup.close();
	$.destroy();
}

function logout() {
	Ti.App.fireEvent('settings:closed');
	passwordKeychainItem.valueData = null;
	Titanium.App.Properties.removeProperty("email");
	Titanium.App.Properties.removeProperty("token");
	Titanium.App.Properties.setString("app:mode", "online");
	var db = Ti.Database.open('_alloy_');
	db.execute('DELETE FROM guestlist');
	db.close();
	$.popup.close();
	$.destroy();
}
