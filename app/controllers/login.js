if (OS_ANDROID) {
	$.popup.addEventListener('android:back', function(e) {
		return false;
	});
}

$.email.addEventListener('return', function() {
	$.password.focus();
});

$.loading.hide();

$.password.addEventListener('change', function(e) {
	if (e.value.length > 0) {
		$.submit.enabled = true;
		$.submit.backgroundColor = '#F77806';
		$.submit.color = "white";
	} else {
		$.submit.enabled = false;
		$.submit.backgroundColor = '#CCCCCC';
		$.submit.color = "white";
	}
});

function appLogin() {
	if (!$.email.value || !$.password.value) {
		dialogs.confirm({
			title : "Error",
			message : "Both email address and password must be entered."
		});
		return;
	}
	$.loading.show();
	var user = Alloy.createModel('user', {
		email : $.email.value,
		password : $.password.value
	});
	user.save({}, {
		success : function(model, response) {
			$.loading.hide();
			Ti.API.info('Login Success');
			Alloy.Globals.passKeychainItem.valueData = $.password.value;
			Titanium.App.Properties.setString("email", $.email.value);
			Titanium.App.Properties.setString("token", response.authentication_token);
			$.popup.close();
			Ti.App.fireEvent('app:login');
		},
		error : function(model, response) {
			console.log(response);
			Ti.API.info('Login error');
			$.loading.hide();
			var dialog = Ti.UI.createAlertDialog({
				message : response.errors[0],
				title : "Login Failed",
			}).show();
		}
	});
}
