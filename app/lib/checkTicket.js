module.exports = function(id, cb) {
    var url = Alloy.Globals.getApi() + "events/" + Titanium.App.Properties.getInt("app:event") + "/check";
    var data = {
        serial : id,
        user : Titanium.App.Properties.getString("email"),
        token : Titanium.App.Properties.getString("token")
    };
    var client = Ti.Network.createHTTPClient({
        onload : function(e) {
            cb(JSON.parse(this.responseText));
        },
        onerror : function(e) {
            cb(null, e.error);
            Ti.API.debug(e.error);
        },
        timeout : 60000
    });
    client.open("POST", url);
    client.send(data);
};
