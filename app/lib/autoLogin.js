exports.login = function() {
    Ti.API.info('auto login');
    var user = Alloy.createModel('user', {
        email : Titanium.App.Properties.getString("email"),
        password : Alloy.Globals.passKeychainItem.valueData
    });
    user.save({}, {
        success : function(model, response) {
            Ti.API.info('Success');
            Titanium.App.Properties.setString("token", response.authentication_token);
            Ti.App.fireEvent('app:login');
        },
        error : function(model, response) {
            Ti.API.info('error');
            Ti.API.info(model);
            var dialog = Ti.UI.createAlertDialog({
                message : response.errors[0],
                title : "Login Failed",
            }).show();
        }
    });
};