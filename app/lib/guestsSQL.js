exports.save = function(data, event) {
    Ti.API.info('data to save');
    
    var db = Ti.Database.open('pftpDatabase');
    db.execute('BEGIN');
    // begin the transaction
    for (var i = 0,  j = data.length; i < j; i++) {
        var item = data[i];
        var checked;
        if (item.checked == "<null>") {
            checked = 0;
        } else {
            checked = item.checked;
        }
        db.execute('INSERT INTO guests (id, first_name, last_name, quantity, serial_number, checked, event_id) VALUES (?, ?, ?, ?, ?, ?, ?)', item.id, item.first_name, item.last_name, item.quantity, item.serial_number, checked, event);
    }
    db.execute('COMMIT');
    db.close();
}; 