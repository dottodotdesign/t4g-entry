var Alloy = require('alloy');

exports.definition = {  
    config: {
        "URL": Alloy.CFG.API + "users/sign_in",
        columns: {
            "email": "string",
            "password": "string",
            "token": "string"
        },
        "debug": 0, 
        "adapter": {
            "type": "restapi",
            "collection_name": "user",
            "idAttribute": "id"
        }
    },      
    extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },  
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }       
};