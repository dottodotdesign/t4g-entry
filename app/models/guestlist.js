exports.definition = {
    config : {
        "URL" : Alloy.CFG.API + "events/" + Titanium.App.Properties.getInt("app:event") + "/guestlist",
        columns : {
            "first_name" : "TEXT",
            "last_name" : "TEXT",
            "quantity" : "integer",
            "event_id" : "integer",
            "item_id" : "integer",
            "ticket_type" : "TEXT",
            "serial_number" : "TEXT PRIMARY KEY",
            "checked": "integer"
        },
        "adapter" : {
            "type" : "sqlrest",
            "collection_name" : "guestlist",
            "idAttribute" : "serial_number"
        },
        "deleteAllOnFetch": true,
        "parentNode" : "guestlist"
    },
    extendModel : function(Model) {
        _.extend(Model.prototype, {
        });
        return Model;
    },
    extendCollection : function(Collection) {
        _.extend(Collection.prototype, {
        	comparator : function(guest) {
                return guest.get('first_name').toLowerCase() + guest.get('last_name').toLowerCase();
            }
        });
        return Collection;
    }
};
