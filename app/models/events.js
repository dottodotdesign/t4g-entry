var Alloy = require('alloy');

exports.definition = {  
    config: {
        "URL": Alloy.CFG.API + "events",
        columns: {
        	"id":"INTEGER PRIMARY KEY",
            "title": "TEXT",
            "start_date": "TEXT",
            "venue": "TEXT",
            "tickets_sold": "integer"
        },
        //"debug": 1, 
        "adapter": {
            "type": "sqlrest",
            "collection_name": "events",
            "idAttribute": "id"
        },
        "returnErrorResponse" : true,
        "deleteAllOnFetch": true,
        "parentNode": "events"
    },      
    extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },  
    extendCollection: function(Collection) {   
        _.extend(Collection.prototype, {});
        return Collection;
    }       
};