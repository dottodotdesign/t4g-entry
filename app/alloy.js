Alloy.Collections.guestlist = Alloy.createCollection('guestlist');
Alloy.Collections.events = Alloy.createCollection('events');

// ADD COLUMN TO A TABLE
var addColumn = function(dbname, tblName, newFieldName, colSpec) {
	var db = Ti.Database.open(dbname);
	var fieldExists = false;
	resultSet = db.execute('PRAGMA TABLE_INFO(' + tblName + ')');
	while (resultSet.isValidRow()) {
		if (resultSet.field(1) == newFieldName) {
			fieldExists = true;
		}
		resultSet.next();
	}
	if (!fieldExists) {
		// field does not exist, so add it
		db.execute('ALTER TABLE ' + tblName + ' ADD COLUMN ' + newFieldName + ' ' + colSpec);
	}
	db.close();
};
addColumn("_alloy_", "guestlist", "ticket_type", "TEXT");
addColumn("_alloy_", "guestlist", "item_id", "TEXT");

Alloy.Globals.getApi = function() {
	return Alloy.CFG.API;
};

var keychain = require('com.obscure.keychain');
Alloy.Globals.passKeychainItem = keychain.createKeychainItem('password');

if (!Titanium.App.Properties.getString("app:mode")) {
	Titanium.App.Properties.setString("app:mode", "online");
}

function getnetworkstate() {
	if (Titanium.Network.online) {
		Ti.API.info('Online');
		Titanium.App.Properties.setBool('app:online', false);
	} else {
		Ti.API.info('Offline');
		Titanium.App.Properties.setBool('app:online', true);
	}
}

getnetworkstate();

Ti.App.addEventListener('resumed', function(e) {
	getnetworkstate();
});

Ti.Network.addEventListener('change', function(e) {
	getnetworkstate();
});
